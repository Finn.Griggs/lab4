package datastructure;


import cellular.CellState;


public class CellGrid implements IGrid {
    public int rows;
    public int columns;
    public CellState[][] cellState;

    public CellGrid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.cellState = new CellState[rows][columns];

    }

    
    public CellGrid(int rows, int columns, CellState initialState) { // Endret på
		this(rows, columns); 
        
        for(int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellState[i][j] = initialState;
            }
        }
	}


    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }


    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException {
        if (row < 0 || row > numRows()) {
            throw new IndexOutOfBoundsException();
        }

        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }

        cellState[row][column] = element;
            
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException {
        if (row < 0 || row > numRows()) {
            throw new IndexOutOfBoundsException();
        }

        if (column < 0 || column > numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        
        return cellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(rows, columns);


        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.columns; j++) {
                gridCopy.set(i, j, get(i, j));
            }
        }
        
        return gridCopy;
    }
    
}
