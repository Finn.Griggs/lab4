package cellular;

public class BriansBrain extends GameOfLife{

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
        
    }


    @Override
    public CellState getNextCell(int row, int col) { 
        CellState currentState = currentGeneration.get(row, col);

		int alive = countNeighbors(row, col, CellState.ALIVE);

	
		if(currentState == CellState.ALIVE) {
			return CellState.DYING;
		} else if(currentState == CellState.DYING) {
			return CellState.DEAD;
			
		} else {
            if(alive == 2) {
                return CellState.ALIVE;
            } else {
                return CellState.DEAD;
            }
        }

    }
    
}